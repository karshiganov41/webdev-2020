**Wiki** - бесплатная энциклопедия без рекламы или оплаты. Английская версия. Поиск информации об исторических фактах, известных людях, событиях, публикациях. Для всех пользователей имеется возможность вносить и корректировать информацию без регистрации.


**Library genesis** -  веб-сайт, поисковая система и онлайн-хранилище, предоставляющее бесплатный доступ к книгам и статьям различной тематики. Сайт создан и базируется в России.


**IDE** - интегрированная среда разработки (англ. Integrated Development Environment) — система программных средств, используемая программистами для разработки программного обеспечения. IDE (англ. Integrated Drive Electronics) — параллельный интерфейс подключения накопителей (жёстких дисков и оптических приводов) к компьютеру. Разработан в 1986 году фирмой Western Digital, позднее стал именоваться ATA, затем PATA.


**GIT** - Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.


**vscode** - Visual Studio Code is a code editor redefined and optimized for building and debugging modern web and cloud applications. Visual Studio Code is free.


**phpstorm**- PhpStorm is perfect for working with Symfony, Laravel, Drupal, WordPress, Zend Framework, Magento, Joomla!, CakePHP, Yii, and other frameworks. All the PHP tools. The editor actually 'gets' your code and deeply understands its structure, supporting all the PHP language features for modern and legacy projects.


**idea** -  Интегрированная среда разработки программного обеспечения для многих языков программирования, в частности Java, JavaScript, Python, разработанная компанией JetBrains. Первая версия появилась в январе 2001 года и быстро приобрела популярность как первая среда для Java с широким набором интегрированных инструментов для рефакторинга, которые позволяли программистам быстро реорганизовывать исходные тексты программ.


**Google Sites** -  бесплатный конструктор сайтов от Google.Инструмент обладает простым интерфейсом, содержащим все самое необходимое и его можно легко подключить к любому другому продукту Google, как например, Google Analytics и AdSense.


**GitLab Pages** - To use GitLab Pages, you must create a project in GitLab to upload your website’s files to. These projects can be either public, internal, or private. GitLab always deploys your website from a very specific folder called public in your repository.


**Сайт-визитка** - небольшой сайт, как правило, состоящий из одной (или нескольких) веб-страниц и содержащий основную информацию об организации, частном лице, компании.


**Фронтэнд (англ. frontend)** — клиентская сторона пользовательского интерфейса к программно-аппаратной части сервиса. Бэкэнд (англ. backend)— программно-аппаратная часть сервиса. Фронт- и бэкэнд — это вариант архитектуры программного обеспечения.


**DNS** (англ. Domain Name System «система доменных имён») — компьютерная распределённая система для получения информации о доменах.


**domain** (tcpip) - TCP/IP — сетевая модель передачи данных, представленных в цифровом виде. Модель описывает способ передачи данных от источника информации к получателю.


**Адаптивный сайт** -  Адаптивность сайта, или по-другому, адаптивный веб-дизайн - это подход, при котором создается веб-страница, которая «реагирует» или изменяет свой размер в зависимости от типа устройства, через которое она просматривается.


**Конструктор сайтов** -  программно реализованная сложная система для создания веб-страниц без знания языков программирования. Обычно является отдельным сервисом, но может и предоставляться как дополнительная услуга хостинг-компаниями.


**Freemium** -  бизнес-модель, которая заключается в предложении воспользоваться компьютерной игрой, программным продуктом, онлайн-сервисом или услугой бесплатно.


**CMS** - информационная система или компьютерная программа, используемая для обеспечения и организации совместного процесса создания, редактирования и управления содержимым, иначе — контентом.


**Figma** is a vector graphics editor and prototyping tool which is primarily web-based, with additional offline features enabled by desktop applications for macOS and Windows. The Figma Mirror companion apps for Android and iOS allow viewing Figma prototypes on mobile devices. The feature set of Figma focuses on use in user interface and user experience design, with an emphasis on real-time collaboration.


**Zeplin** - это инструмент совместной работы для разработчиков пользовательского интерфейса и front-end разработчиков.


**DDNS** - сервис динамических DNS, который подменяет ваш меняющийся динамический IP на постоянный доменный адрес.


**Port-forwarding** - это технология, которая позволяет подключаться (обращаться) через Интернет к компьютеру расположенному во внутренней локальной сети т.е за маршрутизатором, использующим NAT (NAPT).


**Virtual-server** - VPS (англ. virtual private server) или VDS (англ. virtual dedicated server), виртуальный выделенный сервер — услуга предоставления в аренду так называемого виртуального выделенного сервера. В плане управления операционной системой по большей части она соответствует физическому выделенному серверу.


**DMZ** - сегмент сети, содержащий общедоступные сервисы и отделяющий их от частных.


**SaaS(англ. software as a service** — программное обеспечение как услуга; также англ. software on demand — программное обеспечение по требованию) — одна из форм облачных вычислений, модель обслуживания, при которой подписчикам предоставляется готовое прикладное программное обеспечение, полностью обслуживаемое провайдером.


**CMS (система управления контентом)** - приложение для управления содержимым динамического сайта.


**WordPress (WP, WordPress.org)** is a free and open-source content management system (CMS) written in PHP[4] and paired with a MySQL or MariaDB database.


**Joomla** is a free and open-source content management system (CMS) for publishing web content, developed by Open Source Matters, Inc. It is built on a model–view–controller web application framework that can be used independently of the CMS.


**Drupal** is a free and open-source web content management framework written in PHP and distributed under the GNU General Public License.Drupal provides a back-end framework for at least 2.3% of all websites worldwide – ranging from personal blogs to corporate, political, and government sites.Systems also use Drupal for knowledge management and for business collaboration.


**OpenCart** is an online store management system. It is PHP-based, using a MySQL database and HTML components. Support is provided for different languages and currencies. It is freely available under the GNU General Public License. As of May 2016, 342,000 websites were using OpenCart.
In computer graphical user interfaces,drag and drop is a pointing device gesture in which the user selects a virtual object by "grabbing" it and dragging it to a different location or onto another virtual object. In general, it can be used to invoke many kinds of actions, or create various types of associations between two abstract objects.


**Elementor** is an Israeli software company, providing web development services. The Elementor Website builder allows WordPress users to create and edit websites by employing the drag and drop technique, with a built-in responsive mode.


A graphical widget (also graphical control element or control) in a graphical user interface is an element of interaction, such as a button or a scroll bar. Controls are software components that a computer user interacts with through direct manipulation to read or edit information about an application. User interface libraries such as Windows Presentation Foundation, GTK, and Cocoa, contain a collection of controls and the logic to render these.


**Хостинг (англ. hosting)** — услуга по предоставлению ресурсов для размещения информации на сервере, постоянно имеющем доступ к сети (обычно Интернет).


**WooCommerce** is an open-source e-commerce plugin for WordPress. It is designed for small to large-sized online merchants using WordPress. Launched on September 27, 2011,[3] the plugin quickly became popular for its simplicity to install and customize and free base product.


**OpenCart** is an online store management system. It is PHP-based, using a MySQL database and HTML components. Support is provided for different languages and currencies. It is freely available under the GNU General Public License. As of May 2016, 342,000 websites were using OpenCart.
